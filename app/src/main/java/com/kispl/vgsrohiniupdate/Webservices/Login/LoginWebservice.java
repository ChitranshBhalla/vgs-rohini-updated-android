package com.kispl.vgsrohiniupdate.Webservices.Login;

import android.content.Context;
import android.util.Log;

import com.kispl.vgsrohiniupdate.Constants.Constant;
import com.kispl.vgsrohiniupdate.PrefrenceHelper.Configuration;
import com.kispl.vgsrohiniupdate.RestApiCall.RestApiCall;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Created by Rishabh on 29/10/18.
 */
public class LoginWebservice
{
    String status = "";
    String msg = "";
    public String result = "";
    Context context;
    private final String TAG = "LoginWebservice.java";

    public String getLoginDetails(Context context,String username,String pass,String deviceType,String deviceToken)
    {
        this.context = context;
        try
        {
            String res = fetRecord(username,pass,deviceType,deviceToken);
            status = validate(res);

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return status;
    }
    private String fetRecord(String username, String pass,String deviceType,String deviceToken)
    {
        String text = "";
        try
        {
            String URL = "username="+username+"&password="+pass+"&deviceType="+deviceType+"&deviceToken="+deviceToken;
            text = RestApiCall.serverRequest(URL, Constant.LOGIN);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return text;
    }
    private String validate(String strValue)
    {
        msg = strValue;
        JSONParser jsonP = new JSONParser();
        try
        {
            Object obj = jsonP.parse(strValue);
            JSONArray jsonArrayObject = (JSONArray) obj;
            JSONObject jsonObject = (JSONObject) jsonP.parse(jsonArrayObject.get(0).toString());
            result=jsonObject.get("result").toString();

            if (result.equals("success"))
            {
                String name=jsonObject.get("name").toString();
                String email=jsonObject.get("email").toString();
                String type=jsonObject.get("usertype").toString();
                String userId=jsonObject.get("loginuserID").toString();
                String image=jsonObject.get("photo").toString();
                String uniqueName=jsonObject.get("username").toString();
//                String blog=jsonObject.get("blog").toString();

                Configuration.setSharedPrefrenceValue(context,Constant.PREFS_NAME,Constant.userID,userId);
                Configuration.setSharedPrefrenceValue(context,Constant.PREFS_NAME,Constant.uniqueName,name);
                Configuration.setSharedPrefrenceValue(context,Constant.PREFS_NAME,Constant.userType,type);
                Configuration.setSharedPrefrenceValue(context,Constant.PREFS_NAME,Constant.userImage,image);
                Configuration.setSharedPrefrenceValue(context,Constant.PREFS_NAME,Constant.userName,uniqueName);
                Configuration.setSharedPrefrenceValue(context,Constant.PREFS_NAME,Constant.userEmail,email);


            }
            else
            {
                Log.e(TAG,"validate_error");
            }


        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }


}
