package com.kispl.vgsrohiniupdate.Webservices.Logout;

import android.content.Context;

import com.kispl.vgsrohiniupdate.Constants.Constant;
import com.kispl.vgsrohiniupdate.RestApiCall.RestApiCall;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Created by Rishabh on 29/10/18.
 */
public class LogoutWebservice
{
    String status = "";
    String msg = "";
    public String result = "";
    Context context;

    public String logout(String userName,String userType) {
        this.context = context;
        try {
            String result = fetRecord(userName,userType);
            status = validate(result);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    private String fetRecord(String userName,String userType) {
        String text = null;
        try {
            String URL = "usertype="+userType+"&username="+userName;
            text = RestApiCall.serverRequest(URL, Constant.LOGOUT);
        } catch (Exception e) {
            System.out.println("in web services catch block");
            e.printStackTrace();
            return e.getLocalizedMessage();
        }
        return text;
    }


    public String validate(String strValue) {
        msg = strValue;
        System.out.println("the value of strValue===  " + strValue);
        Long status;
        JSONParser jsonP = new JSONParser();
        try {
            Object obj = jsonP.parse(strValue);
            JSONArray jsonArrayObject = (JSONArray) obj;
            JSONObject jsonObject = (JSONObject) jsonP.parse(jsonArrayObject.get(0).toString());
            result=jsonObject.get("result").toString();


        } catch (Exception e) {
            System.out.println("in second catch block");
            e.printStackTrace();
        }
        return result;
    }

}
