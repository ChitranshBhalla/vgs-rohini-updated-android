package com.kispl.vgsrohiniupdate.Webservices.NewsWebServices;

import android.util.Log;

import com.kispl.vgsrohiniupdate.Beans.NewsBeans.NewsBeans;
import com.kispl.vgsrohiniupdate.Constants.Constant;
import com.kispl.vgsrohiniupdate.RestApiCall.RestApiCall;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;



/**
 * Created by Rishabh on 30/10/18.
 */
public class NewsWebservice
{
    private String status = "";
    public String getNewsDetail()
    {
        try
        {
            String res = fetRecord();
            status = validate(res);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return status;
    }
    private String fetRecord()
    {
        String msg= "";
        try
        {
            msg = RestApiCall.serverRequest("",Constant.NEWS);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return msg;
    }
    private String validate(String strValue)
    {
        String result="";
        JSONParser jsonP = new JSONParser();
        try
        {
            Object obj = jsonP.parse(strValue);
            JSONArray jsonArrayObject = (JSONArray) obj;
            JSONObject jsonObject = (JSONObject) jsonP.parse(jsonArrayObject.get(0).toString());
            result=jsonObject.get("result").toString();
            if(result.equals("success"))
            {
                //                [{"result":"success","news":[{"newsID":"","headlines":"","title":"","image":"","news":"}]}]
                String news  = jsonObject.get("news").toString();

                getNewsData(news);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return "success";
    }

    private String getNewsData(String data)
    {
        JSONParser jsonParser = new JSONParser();
        try
        {
            Object object = jsonParser.parse(data);
            JSONArray jsonArray = (JSONArray)object;

            for (int i=0;i<jsonArray.size();i++)
            {
                JSONObject jsonObject = (JSONObject)jsonParser.parse(jsonArray.get(i).toString());


                    NewsBeans.newstitleArray.add(jsonObject.get("title").toString());
                    NewsBeans.newsImageArray.add(jsonObject.get("image").toString());
                    NewsBeans.newsIDArray.add(jsonObject.get("newsID").toString());
                    NewsBeans.newsDateArray.add(jsonObject.get("added_on").toString());
                    NewsBeans.newsHeadlinesArray.add(jsonObject.get("headlines").toString());
                    NewsBeans.newsDetailArray.add(jsonObject.get("news").toString());
                    Log.e("Image : ","is - > "+jsonObject.get("image").toString());

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return "pp";
    }

}
