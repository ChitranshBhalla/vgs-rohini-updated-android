package com.kispl.vgsrohiniupdate.Utlities;

import android.os.Build;
import android.text.Html;

/**
 * Created by Rishabh on 31/10/18.
 */
public class HelpingFunctions {

    public static String htmlToText(String data)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return String.valueOf(Html.fromHtml(data, Html.FROM_HTML_MODE_COMPACT));
        }
        else {
            return String.valueOf(Html.fromHtml(data));
        }

    }
}
