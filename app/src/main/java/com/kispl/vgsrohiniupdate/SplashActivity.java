package com.kispl.vgsrohiniupdate;

import android.content.Intent;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.kispl.vgsrohiniupdate.Activities.Login.LoginActivity;
import com.kispl.vgsrohiniupdate.Activities.Teacher.Dashboard.TeacherDashboardActivity;
import com.kispl.vgsrohiniupdate.Constants.Constant;
import com.kispl.vgsrohiniupdate.PrefrenceHelper.Configuration;

public class SplashActivity extends AppCompatActivity {

    String userType = "";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        userType = Configuration.getSharedPrefrenceValue(SplashActivity.this,Constant.userType);
        splashScreen();
    }
    public void splashScreen()
    {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run()
            {

                if(userType != null)
                {
                    if(userType.equals("Teacher"))
                    {
                        startActivity(new Intent(SplashActivity.this,TeacherDashboardActivity.class));
                    }
                    else
                    {
                        startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                    }
                }
                else
                {
                    startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                }



            }
        },900);
    }
}
