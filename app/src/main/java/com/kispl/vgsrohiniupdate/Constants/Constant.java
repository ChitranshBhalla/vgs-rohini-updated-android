package com.kispl.vgsrohiniupdate.Constants;

/**
 * Created by Rishabh on 29/10/18.
 */
public class Constant
{
    public static final String PREFS_NAME = "prefs_name";
    public static String WS_HTTP="Http";
    public static String WS_DOMAIN_NAME="skoolroom.co.in";
    public static String WS_PATH="/vgs_test/webservice/";
    public static String WS_SUCCESS="success";

// DEVELOPMENT "http://skoolroom.co.in/development/webservice/"
// VGS_TEST    "http://skoolroom.co.in/vgs_test/webservice/"
//VGS _ ROHINI "http://skoolroom.in/vgs_rohini/webservice/"
// SVIS        "http://skoolroom.in/svis/webservice/"

//      ---------- *** User *** -----------

    public static final String userName = "username";
    public static final String userID = "userID";
    public static final String userType = "usertype";
    public static final String uniqueName = "unique_Name";
    public static final String userImage = "userImage";
    public static final String userEmail = "userEmail";
    public static final String deviceToken = "devicetoken";


//      ----------- *** WEBSERVICES *** ----------

    public static String LOGIN="login";
    public static String LOGOUT = "logout";
    public static String NEWS = "news";
}
