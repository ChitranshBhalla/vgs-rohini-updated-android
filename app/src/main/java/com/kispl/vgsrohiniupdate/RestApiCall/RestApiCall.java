package com.kispl.vgsrohiniupdate.RestApiCall;

import android.util.Log;

import com.kispl.vgsrohiniupdate.Constants.Constant;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

/**
 * Created by Rishabh on 29/10/18.
 */
public class RestApiCall
{
    public static String serverRequest(String URL, String WS_Name)
    {
        String text="";
        try
        {
            URI uri = new URI(Constant.WS_HTTP, Constant.WS_DOMAIN_NAME,Constant.WS_PATH+WS_Name,URL, null);
            String ll=uri.toASCIIString();
            java.net.URL obj = new URL(ll);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            Log.d("URL-->", ll);
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type","text/html; charset=iso-8859-1");
            System.setProperty("http.keepAlive", "false");


            int responseCode = con.getResponseCode();

            Log.d("Response Code-->", responseCode + "");
            Log.d("Content Type-->",con.getContentType()+"");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null)
            {
                response.append(inputLine);
            }
            in.close();

            text=response.toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Log.d("Response JSON-->",text);
        return text;
    }
}
