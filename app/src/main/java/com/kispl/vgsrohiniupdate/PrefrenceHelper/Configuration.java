package com.kispl.vgsrohiniupdate.PrefrenceHelper;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.WindowManager;

import com.kispl.vgsrohiniupdate.Constants.Constant;
import com.kispl.vgsrohiniupdate.R;

/**
 * Created by Rishabh on 29/10/18.
 */
public class Configuration
{
    public static void  setSharedPrefrenceValue(Object objCurrentClassReference, String strPrefenceFileName,
                                                String strKey, String strValue)
    {

        SharedPreferences sharedPreferencesForStoringData = ((ContextWrapper) objCurrentClassReference)
                .getSharedPreferences(strPrefenceFileName, 0);
        SharedPreferences.Editor sharedPrefencesEditor = sharedPreferencesForStoringData
                .edit();

        sharedPrefencesEditor.putString(strKey, strValue);
        sharedPrefencesEditor.apply();


    }

    public static void removeSharedPrefrenceValue(Object objCurrentClassReference,String strPrefenceFileName,String strKey)
    {
        SharedPreferences sharedPreferences = ((ContextWrapper)objCurrentClassReference).getSharedPreferences(strPrefenceFileName,0);
        SharedPreferences.Editor sharedEditor = sharedPreferences.edit();
        sharedEditor.remove(strKey);
        sharedEditor.apply();

    }

    public static String getSharedPrefrenceValue(Object objCurrentClassReference,String key)
    {

        SharedPreferences settings = ((ContextWrapper) objCurrentClassReference)
                .getSharedPreferences(
                        Constant.PREFS_NAME, 0);
        return settings.getString(key, null);

    }
    public static boolean isInternetConnection(Context context)
    {

        boolean statusInternet;
        ConnectivityManager cn = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

        NetworkInfo nf=cn.getActiveNetworkInfo();
        if(nf != null && nf.isConnected())
        {
            Log.i("Info:", "Network Available.");
            statusInternet=true;
        }
        else
        {
            Log.i("Info:", "Network Not Available.");
            statusInternet=false;

        }
        return statusInternet;
    }

    public static ProgressDialog createProgressDialog(Context context)
    {
        ProgressDialog dialog = new ProgressDialog(context);
        try
        {
            dialog.show();
        }
        catch (WindowManager.BadTokenException e)
        {

        }
        dialog.setCancelable(false);
        dialog.getWindow()
                .setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.progressdialog);

        return dialog;
    }
}
