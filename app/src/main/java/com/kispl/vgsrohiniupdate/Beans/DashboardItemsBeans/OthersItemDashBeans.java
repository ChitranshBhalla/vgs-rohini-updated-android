package com.kispl.vgsrohiniupdate.Beans.DashboardItemsBeans;



/**
 * Created by Rishabh on 30/10/18.
 */
public class OthersItemDashBeans
{
    private String title,detailText;
    private int img;

    public OthersItemDashBeans(String title, String detailText, int img) {
        this.title = title;
        this.detailText = detailText;
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetailText() {
        return detailText;
    }

    public void setDetailText(String detailText) {
        this.detailText = detailText;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
