package com.kispl.vgsrohiniupdate.Beans.NoticeBeans;

import java.util.ArrayList;

/**
 * Created by Rishabh on 29/10/18.
 */
public class NoticeBeans
{
    private String noticeTitle,noticeAddedDate,noticeID;

    public NoticeBeans(String noticeTitle, String noticeAddedDate, String noticeID)
    {
        this.noticeTitle = noticeTitle;
        this.noticeAddedDate = noticeAddedDate;
        this.noticeID = noticeID;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    public String getNoticeAddedDate() {
        return noticeAddedDate;
    }

    public void setNoticeAddedDate(String noticeAddedDate) {
        this.noticeAddedDate = noticeAddedDate;
    }

    public String getNoticeID() {
        return noticeID;
    }

    public void setNoticeID(String noticeID) {
        this.noticeID = noticeID;
    }

    public static ArrayList<String> noticeTitleArray = new ArrayList<>();
    public static ArrayList<String> noticeAddedDateArray = new ArrayList<>();
    public static ArrayList<String> noticeIDArray = new ArrayList<>();
}
