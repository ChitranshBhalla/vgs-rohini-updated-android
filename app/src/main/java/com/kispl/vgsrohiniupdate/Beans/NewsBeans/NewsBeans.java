package com.kispl.vgsrohiniupdate.Beans.NewsBeans;

import java.util.ArrayList;

/**
 * Created by Rishabh on 29/10/18.
 */
public class NewsBeans
{
    private String newsTitle,newsID,newsDate,newsImage,newsHeadlines,newsDetail;

    public NewsBeans(String newsTitle, String newsID, String newsDate,String newsImage,String newsHeadlines,String newsDetail) {
        this.newsTitle = newsTitle;
        this.newsID = newsID;
        this.newsDate = newsDate;
        this.newsImage = newsImage;
        this.newsHeadlines = newsHeadlines;
        this.newsDetail = newsDetail;
    }

    public String getNewsDetail() {
        return newsDetail;
    }

    public void setNewsDetail(String newsDetail) {
        this.newsDetail = newsDetail;
    }

    public String getNewsImage() {
        return newsImage;
    }

    public void setNewsImage(String newsImage) {
        this.newsImage = newsImage;
    }

    public String getNewsHeadlines() {
        return newsHeadlines;
    }

    public void setNewsHeadlines(String newsHeadlines) {
        this.newsHeadlines = newsHeadlines;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsID() {
        return newsID;
    }

    public void setNewsID(String newsID) {
        this.newsID = newsID;
    }

    public String getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(String newsDate) {
        this.newsDate = newsDate;
    }

    public static ArrayList<String> newstitleArray = new ArrayList<>();
    public static ArrayList<String> newsIDArray =new ArrayList<>();
    public static ArrayList<String> newsDateArray = new ArrayList<>();
    public static ArrayList<String> newsImageArray=new ArrayList<>();
    public static ArrayList<String> newsHeadlinesArray=new ArrayList<>();
    public static ArrayList<String>newsDetailArray = new ArrayList<>();
}
