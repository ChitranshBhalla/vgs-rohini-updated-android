package com.kispl.vgsrohiniupdate.Adapters.NoticeAdapters.NoticeListAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kispl.vgsrohiniupdate.Beans.NoticeBeans.NoticeBeans;
import com.kispl.vgsrohiniupdate.R;

import java.util.ArrayList;

/**
 * Created by Rishabh on 30/10/18.
 */
public class NoticeListAdapter extends RecyclerView.Adapter<NoticeListAdapter.MyViewHolder>
{
    private Context context;
    private ArrayList<NoticeBeans> data;

    public NoticeListAdapter(Context context, ArrayList<NoticeBeans> data)
    {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.notice_list_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        holder.titleText.setText(data.get(i).getNoticeTitle());
        holder.dateText.setText(data.get(i).getNoticeAddedDate());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView monthText,dateText,titleText;
        private MyViewHolder(View itemView)
        {
            super(itemView);
            monthText = itemView.findViewById(R.id.notice_list_month_text);
            dateText = itemView.findViewById(R.id.notice_list_date_text);
            titleText = itemView.findViewById(R.id.notice_list_title);
        }
    }
}
