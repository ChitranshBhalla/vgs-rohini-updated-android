package com.kispl.vgsrohiniupdate.Adapters.TeacherAdapter.TeacherMainDashboardAdapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kispl.vgsrohiniupdate.Activities.News.NewsList.NewsListActivity;
import com.kispl.vgsrohiniupdate.Activities.Notice.NoticeList.NoticeListActivity;
import com.kispl.vgsrohiniupdate.Adapters.NewsAdapter.NewsDashboardAdapter.NewsDashboardAdapter;
import com.kispl.vgsrohiniupdate.Adapters.NoticeAdapters.NoticeDashBoardAdapter.NoticeDashboardAdapter;
import com.kispl.vgsrohiniupdate.Adapters.OtherDashboardAdapter.OtherDashboardAdapter;
import com.kispl.vgsrohiniupdate.Beans.DashboardItemsBeans.OthersItemDashBeans;
import com.kispl.vgsrohiniupdate.Beans.NewsBeans.NewsBeans;
import com.kispl.vgsrohiniupdate.Beans.NoticeBeans.NoticeBeans;
import com.kispl.vgsrohiniupdate.R;

import java.util.ArrayList;

import static com.kispl.vgsrohiniupdate.Activities.Teacher.Dashboard.TeacherDashboardActivity.getNewsData;
import static com.kispl.vgsrohiniupdate.Activities.Teacher.Dashboard.TeacherDashboardActivity.getNoticeData;
import static com.kispl.vgsrohiniupdate.Activities.Teacher.Dashboard.TeacherDashboardActivity.getOtherDashData;

/**
 * Created by Rishabh on 29/10/18.
 */
public class TeacherMainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private Context context;
    private ArrayList<Object> items;

    private final int NOTICE_BOARD_LIST = 1;
    private final int NEWS_BOARD_LIST = 2;
    private final int OTHER_BOARD_LIST = 3;

    public TeacherMainAdapter(Context context, ArrayList<Object> items) {
        this.context = context;
        this.items = items;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view;
        RecyclerView.ViewHolder holder = null;
        switch (viewType)
        {
            case NOTICE_BOARD_LIST:
                view = inflater.inflate(R.layout.notice_dash_adapter,viewGroup,false);
                holder = new NoticeListHolder(view);
                break;
            case NEWS_BOARD_LIST:
                view = inflater.inflate(R.layout.news_dash_adapter,viewGroup,false);
                holder = new NewsListHolder(view);
                break;
            case OTHER_BOARD_LIST:
                view = inflater.inflate(R.layout.other_dash_adapter,viewGroup,false);
                holder = new OtherListHolder(view);
                break;

        }
        return holder;
    }
    @Override
    public int getItemViewType(int position) {
        if(items.get(position) instanceof NoticeBeans)
            return NOTICE_BOARD_LIST;
        else if(items.get(position) instanceof NewsBeans)
            return NEWS_BOARD_LIST;
        else if(items.get(position) instanceof OthersItemDashBeans)
            return OTHER_BOARD_LIST;
        return -1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {
        if(holder.getItemViewType() == NOTICE_BOARD_LIST)
            noticeList((NoticeListHolder)holder);
        if(holder.getItemViewType() == NEWS_BOARD_LIST)
            newsList((NewsListHolder)holder);
        if(holder.getItemViewType() == OTHER_BOARD_LIST)
            otherList((OtherListHolder)holder);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class NoticeListHolder extends RecyclerView.ViewHolder
    {
        RecyclerView recyclerView;
        TextView noticeShowAllText;
        private NoticeListHolder(View itemView)
        {
            super(itemView);
            recyclerView = itemView.findViewById(R.id.notice_recyclerview);
            noticeShowAllText = itemView.findViewById(R.id.notice_show_allButton);

        }
    }
    private void noticeList(NoticeListHolder holder)
    {
        NoticeDashboardAdapter adapter = new NoticeDashboardAdapter(getNoticeData(),context);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayout.HORIZONTAL,false));
        holder.recyclerView.setAdapter(adapter);
        holder.noticeShowAllText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context,NoticeListActivity.class));
            }
        });
    }



    public class NewsListHolder extends RecyclerView.ViewHolder
    {
        RecyclerView recyclerView;
        TextView newsShowAllText;
        public NewsListHolder(View itemView)
        {
            super(itemView);
            recyclerView=itemView.findViewById(R.id.news_recyclerview);
            newsShowAllText=itemView.findViewById(R.id.news_show_allButton);
        }
    }
    private void newsList(NewsListHolder holder)
    {
        NewsDashboardAdapter adapter = new NewsDashboardAdapter(getNewsData(),context);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayout.HORIZONTAL,false));
        holder.recyclerView.setAdapter(adapter);
        holder.newsShowAllText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context,NewsListActivity.class));
            }
        });
    }


    public class OtherListHolder extends RecyclerView.ViewHolder
    {
        RecyclerView recyclerView;

        public OtherListHolder(View itemView)
        {
            super(itemView);
            recyclerView = itemView.findViewById(R.id.others_recycler_view);
        }
    }
    private void otherList(OtherListHolder holder)
    {
        OtherDashboardAdapter adapter = new OtherDashboardAdapter(context,getOtherDashData());
        holder.recyclerView.setAdapter(adapter);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
    }
}
