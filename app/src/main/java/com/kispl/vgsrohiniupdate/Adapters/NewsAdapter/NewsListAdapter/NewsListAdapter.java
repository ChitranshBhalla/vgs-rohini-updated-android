package com.kispl.vgsrohiniupdate.Adapters.NewsAdapter.NewsListAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kispl.vgsrohiniupdate.Activities.News.NewsDetail.NewsDetailActivity;
import com.kispl.vgsrohiniupdate.Beans.NewsBeans.NewsBeans;
import com.kispl.vgsrohiniupdate.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Rishabh on 30/10/18.
 */
public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.MyViewHolder>
{
    private ArrayList<NewsBeans> data;
    private Context context;

    public NewsListAdapter(ArrayList<NewsBeans> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.news_list_main_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int i) {


        holder.title.setText(data.get(i).getNewsTitle());
        holder.addedDate.setText(context.getString(R.string.added_on,data.get(i).getNewsDate()));
        Picasso.with(context).load(data.get(i).getNewsImage()).into(holder.newsImage);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,NewsDetailActivity.class);
                intent.putExtra("news_title",data.get(i).getNewsTitle());
                intent.putExtra("news_date",data.get(i).getNewsDate());
                intent.putExtra("news_image",data.get(i).getNewsImage());
                intent.putExtra("news_detail",data.get(i).getNewsDetail());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView title,addedDate;
        ImageView newsImage;
        ConstraintLayout mainLayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.news_title);
            addedDate = itemView.findViewById(R.id.news_added_date);
            newsImage=itemView.findViewById(R.id.news_image_view);
            mainLayout = itemView.findViewById(R.id.news_main_layout);
        }
    }

}
