package com.kispl.vgsrohiniupdate.Adapters.NoticeAdapters.NoticeDashBoardAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kispl.vgsrohiniupdate.Beans.NoticeBeans.NoticeBeans;
import com.kispl.vgsrohiniupdate.R;

import java.util.ArrayList;

/**
 * Created by Rishabh on 29/10/18.
 */
public class NoticeDashboardAdapter extends RecyclerView.Adapter<NoticeDashboardAdapter.MyViewHolder>
{

    private ArrayList<NoticeBeans> data;
    private Context context;
    private View view;

    public NoticeDashboardAdapter(ArrayList<NoticeBeans> data, Context context) {
        this.data = data;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notice_dash_list_item,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int i) {
        holder.titleLabel.setText(data.get(i).getNoticeTitle());
        holder.dateLabel.setText(data.get(i).getNoticeAddedDate());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView monthLabel,dateLabel,titleLabel;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            monthLabel = itemView.findViewById(R.id.notice_month_text);
            dateLabel = itemView.findViewById(R.id.notice_date_text);
            titleLabel = itemView.findViewById(R.id.notice_title);
        }
    }
}
