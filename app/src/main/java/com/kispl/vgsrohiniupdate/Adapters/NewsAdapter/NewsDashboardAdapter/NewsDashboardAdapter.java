package com.kispl.vgsrohiniupdate.Adapters.NewsAdapter.NewsDashboardAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.kispl.vgsrohiniupdate.Activities.News.NewsDetail.NewsDetailActivity;
import com.kispl.vgsrohiniupdate.Beans.NewsBeans.NewsBeans;
import com.kispl.vgsrohiniupdate.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import okhttp3.OkHttpClient;

/**
 * Created by Rishabh on 30/10/18.
 */
public class NewsDashboardAdapter extends RecyclerView.Adapter<NewsDashboardAdapter.MyViewHolder>
{
    private View view;
    private ArrayList<NewsBeans> data;
    private Context context;

    public NewsDashboardAdapter(ArrayList<NewsBeans> data, Context context)
    {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(context).inflate(R.layout.news_dash_list_item,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int i) {

        holder.newsTitle.setText(data.get(i).getNewsTitle());
        OkHttpClient client = new OkHttpClient();
        Picasso picasso = new Picasso.Builder(context).downloader(new OkHttp3Downloader(client)).build();
//        Picasso.with(context).load(data.get(i).getNewsImage()).into(holder.newsImage);
        picasso.load(data.get(i).getNewsImage()).into(holder.newsImage);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,NewsDetailActivity.class);
                intent.putExtra("news_title",data.get(i).getNewsTitle());
                intent.putExtra("news_date",data.get(i).getNewsDate());
                intent.putExtra("news_image",data.get(i).getNewsImage());
                intent.putExtra("news_detail",data.get(i).getNewsDetail());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView newsImage;
        TextView newsTitle;
        CardView mainLayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            newsImage = itemView.findViewById(R.id.news_image);
            newsTitle = itemView.findViewById(R.id.news_title);
            mainLayout = itemView.findViewById(R.id.main_card_layout);

        }
    }
}
