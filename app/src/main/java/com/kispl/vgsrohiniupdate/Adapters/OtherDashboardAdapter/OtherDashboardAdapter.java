package com.kispl.vgsrohiniupdate.Adapters.OtherDashboardAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kispl.vgsrohiniupdate.Beans.DashboardItemsBeans.OthersItemDashBeans;
import com.kispl.vgsrohiniupdate.R;

import java.util.ArrayList;

/**
 * Created by Rishabh on 30/10/18.
 */
public class OtherDashboardAdapter extends RecyclerView.Adapter<OtherDashboardAdapter.CustomViewHolder> {

    private View view;
    private Context context;
    private ArrayList<OthersItemDashBeans> data;

    public OtherDashboardAdapter(Context context, ArrayList<OthersItemDashBeans> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(context).inflate(R.layout.other_dash_list_item,viewGroup,false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int i) {
        holder.title.setText(data.get(i).getTitle());
        holder.detail.setText(data.get(i).getDetailText());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder
    {
        TextView title,detail;
        public CustomViewHolder(View itemView)
        {
            super(itemView);
            title = itemView.findViewById(R.id.other_dash_title_text);
            detail = itemView.findViewById(R.id.other_dash_detail_text);
        }
    }
}
