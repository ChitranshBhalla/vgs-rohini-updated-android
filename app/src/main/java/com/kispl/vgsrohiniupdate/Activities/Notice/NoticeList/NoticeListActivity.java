package com.kispl.vgsrohiniupdate.Activities.Notice.NoticeList;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.kispl.vgsrohiniupdate.Adapters.NoticeAdapters.NoticeListAdapter.NoticeListAdapter;
import com.kispl.vgsrohiniupdate.R;

import static com.kispl.vgsrohiniupdate.Activities.Teacher.Dashboard.TeacherDashboardActivity.getNoticeData;

/**
 * Created by Rishabh on 29/10/18.
 */
public class NoticeListActivity extends AppCompatActivity
{
    RecyclerView recyclerView;
    private NoticeListAdapter noticeListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_list);

        init();

    }
    private void init()
    {
        Toolbar toolbar= findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.notice_list_recycler_view);

        noticeListAdapter = new NoticeListAdapter(NoticeListActivity.this,getNoticeData());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(NoticeListActivity.this);
        recyclerView.setAdapter(noticeListAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.login_back);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
