package com.kispl.vgsrohiniupdate.Activities.Teacher.Dashboard;


import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.kispl.vgsrohiniupdate.Activities.Logout.Logout;
import com.kispl.vgsrohiniupdate.Adapters.TeacherAdapter.TeacherMainDashboardAdapter.TeacherMainAdapter;
import com.kispl.vgsrohiniupdate.Beans.DashboardItemsBeans.OthersItemDashBeans;
import com.kispl.vgsrohiniupdate.Beans.NewsBeans.NewsBeans;
import com.kispl.vgsrohiniupdate.Beans.NoticeBeans.NoticeBeans;
import com.kispl.vgsrohiniupdate.Constants.Constant;
import com.kispl.vgsrohiniupdate.PrefrenceHelper.Configuration;
import com.kispl.vgsrohiniupdate.R;
import com.kispl.vgsrohiniupdate.Webservices.NewsWebServices.NewsWebservice;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Rishabh on 29/10/18.
 */
public class TeacherDashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private static ArrayList<Object> objects = new ArrayList<>();
    ProgressDialog progressDialog;
    public RecyclerView recyclerView;
    public TeacherMainAdapter teacherMainAdapter;
    public LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_dashboard);

        init();
        setupNavigationSideMenu();


    }
    private void init()
    {
        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        progressDialog = new ProgressDialog(TeacherDashboardActivity.this);
        recyclerView = findViewById(R.id.teach_recycler_view);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ApiCall();
            }
        },10);


    }

    public static ArrayList<Object> getObjects()
    {


            objects.clear();
            if(getNoticeData().size()>0)
            {
                objects.add(getNoticeData().get(0));
            }
            if(getNewsData().size()>0)
            {
                objects.add(getNewsData().get(0));
            }
            if(getOtherDashData().size()>0)
            {
                objects.add(getOtherDashData().get(0));
            }



        return objects;
    }
    public static ArrayList<NoticeBeans> getNoticeData()
    {
        ArrayList<NoticeBeans> noticeBeans = new ArrayList<>();
        for(int i=0;i<10;i++)
        {
            noticeBeans.add(new NoticeBeans("Title"+i,""+i,"ID"+i));
        }
        return noticeBeans;
    }

    public static ArrayList<NewsBeans> getNewsData()
    {

        ArrayList<NewsBeans> newsBeans = new ArrayList<>();
        for(int i=0;i<NewsBeans.newsIDArray.size();i++)
        {
            newsBeans.add(new NewsBeans(NewsBeans.newstitleArray.get(i),NewsBeans.newsIDArray.get(i),NewsBeans.newsDateArray.get(i),NewsBeans.newsImageArray.get(i),NewsBeans.newsHeadlinesArray.get(i),NewsBeans.newsDetailArray.get(i)));
        }
        return newsBeans;
    }

    public static ArrayList<OthersItemDashBeans> getOtherDashData()
    {
        ArrayList<OthersItemDashBeans> othersItemDashBeans = new ArrayList<>();
        othersItemDashBeans.add(new OthersItemDashBeans("Homework","Tap to Check Homework",R.drawable.drawer_indicator));
        othersItemDashBeans.add(new OthersItemDashBeans("Parent Teacher Interaction","Tap to Check PTI",R.drawable.drawer_indicator));
        othersItemDashBeans.add(new OthersItemDashBeans("Notifications","Tap to Check Notifications",R.drawable.drawer_indicator));
        return othersItemDashBeans;
    }

    private void setupNavigationSideMenu()
    {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorPrimaryDark));
        toggle.syncState();
        NavigationView navigationView =findViewById(R.id.nav_view);
        final Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.drawer_indicator, getApplicationContext().getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });

        View hView =  navigationView.getHeaderView(0);
        TextView headerName = hView.findViewById(R.id.teacher_header_name);
        CircleImageView headerImage = hView.findViewById(R.id.teacher_header_imageView);
        String str_Username = Configuration.getSharedPrefrenceValue(TeacherDashboardActivity.this, Constant.uniqueName);
        String str_UserImage = Configuration.getSharedPrefrenceValue(TeacherDashboardActivity.this, Constant.userImage);
        headerName.setText(str_Username);
        Picasso.with(TeacherDashboardActivity.this).load(str_UserImage).into(headerImage);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == R.id.nav_teach_logout)
        {
            String userName= Configuration.getSharedPrefrenceValue(TeacherDashboardActivity.this, Constant.userName);
            String userType=Configuration.getSharedPrefrenceValue(TeacherDashboardActivity.this,Constant.userType);

            if(Configuration.isInternetConnection(TeacherDashboardActivity.this))
            {

                Logout logout = new Logout(TeacherDashboardActivity.this);
                logout.execute(userName,userType);
            }
            else
            {
                Toast.makeText(TeacherDashboardActivity.this,R.string.no_internet,Toast.LENGTH_SHORT).show();
            }
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void ApiCall()
    {
        if(Configuration.isInternetConnection(TeacherDashboardActivity.this))
        {
            NewsApi newsApi = new NewsApi();
            newsApi.execute();
        }
        else {
            Toast.makeText(TeacherDashboardActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        }
    }

    public class NewsApi extends AsyncTask<String ,String ,String >
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage(getResources().getString(R.string.please_wait));
            progressDialog.setTitle(R.string.loading);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            NewsWebservice newsWebservice = new NewsWebservice();
            return newsWebservice.getNewsDetail();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try
            {
                if (s.equals(Constant.WS_SUCCESS))
                {
                    teacherMainAdapter = new TeacherMainAdapter(TeacherDashboardActivity.this, getObjects());
                    linearLayoutManager = new LinearLayoutManager(TeacherDashboardActivity.this);
                    recyclerView.setAdapter(teacherMainAdapter);
                    recyclerView.setLayoutManager(linearLayoutManager);
                }
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            teacherMainAdapter = new TeacherMainAdapter(TeacherDashboardActivity.this, getObjects());
//                            linearLayoutManager = new LinearLayoutManager(TeacherDashboardActivity.this);
//                            recyclerView.setAdapter(teacherMainAdapter);
//                            recyclerView.setLayoutManager(linearLayoutManager);
//                        }
//                    },30);



//               handler.postDelayed(new Runnable() {
//                   @Override
//                   public void run() {
//                        mAdapter.updateHomeDataSet(getObjects());
//                        mAdapter.notifyDataSetChanged();
//                       new FetchCustumUTBList().execute();
////                       progressDialog.dismiss();
//                   }
//               },100);


//                    }

//                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            finally {
                progressDialog.dismiss();
            }
        }
    }

}
