package com.kispl.vgsrohiniupdate.Activities.Login;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kispl.vgsrohiniupdate.Activities.Teacher.Dashboard.TeacherDashboardActivity;
import com.kispl.vgsrohiniupdate.Constants.Constant;
import com.kispl.vgsrohiniupdate.PrefrenceHelper.Configuration;
import com.kispl.vgsrohiniupdate.R;
import com.kispl.vgsrohiniupdate.Webservices.Login.LoginWebservice;

/**
 * Created by Rishabh on 29/10/18.
 */
public class LoginActivity extends AppCompatActivity
{

    private EditText et_username,et_password;
    private Button loginBtn;
    private String str_Username,str_Password;
    public ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        init();
    }
    private void init()
    {
        et_username = findViewById(R.id.username);
        et_password = findViewById(R.id.password);
        loginBtn = findViewById(R.id.login);
        dialog = new ProgressDialog(LoginActivity.this);

        handleBtnClick();
    }
    private void handleBtnClick()
    {
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                str_Username = et_username.getText().toString();
                str_Password = et_password.getText().toString();

                if(str_Username.isEmpty() && str_Password.isEmpty())
                {
                    et_username.requestFocus();
                    et_username.setFocusable(true);
                    et_username.setError(getResources().getString(R.string.check_user));

                    et_password.requestFocus();
                    et_password.setError(getResources().getString(R.string.check_pass));
                    et_password.setFocusable(true);
                }
                else if(str_Username.isEmpty())
                {
                    et_username.requestFocus();
                    et_username.setFocusable(true);
                    et_username.setError(getResources().getString(R.string.check_user));
                }
                else if(str_Password.isEmpty())
                {
                    et_password.requestFocus();
                    et_password.setError(getResources().getString(R.string.check_pass));
                    et_password.setFocusable(true);
                }
                else
                {
                    ApiCall(str_Username,str_Password);
                }
            }
        });
    }

    private void ApiCall(String username,String password)
    {

        if(Configuration.isInternetConnection(LoginActivity.this))
        {
            String deivceToken = Configuration.getSharedPrefrenceValue(LoginActivity.this,Constant.deviceToken);
            String deviceType = "Android";
            new LoginWebServiceCall().execute(username,password, deviceType,deivceToken);
        }
        else
        {
            Toast.makeText(LoginActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        }

    }
    @SuppressLint("StaticFieldLeak")
    private class LoginWebServiceCall extends AsyncTask<String,String,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage(getResources().getString(R.string.please_wait));
            dialog.setTitle(R.string.logging_in);
            dialog.show();
        }

        @Override
        protected String doInBackground(String...s) {
            LoginWebservice loginWebservice = new LoginWebservice();
            return loginWebservice.getLoginDetails(LoginActivity.this,s[0],s[1],s[2],s[3]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try
            {
                if (s.equals(Constant.WS_SUCCESS))
                {
                    String userType = Configuration.getSharedPrefrenceValue(LoginActivity.this,Constant.userType);
                    if(userType.equals("Teacher"))
                    {
                        startActivity(new Intent(LoginActivity.this,TeacherDashboardActivity.class));
                    }
                    else
                    {
                        Toast.makeText(LoginActivity.this, "Coming soon..", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(LoginActivity.this,R.string.check_credits,Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                dialog.dismiss();
            }

        }
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed()
    {
        if (doubleBackToExitPressedOnce)
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
            startActivity(intent);
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.press_back, Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}
