package com.kispl.vgsrohiniupdate.Activities.News.NewsList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.kispl.vgsrohiniupdate.Activities.Notice.NoticeList.NoticeListActivity;
import com.kispl.vgsrohiniupdate.Adapters.NewsAdapter.NewsListAdapter.NewsListAdapter;
import com.kispl.vgsrohiniupdate.Adapters.NoticeAdapters.NoticeListAdapter.NoticeListAdapter;
import com.kispl.vgsrohiniupdate.R;

import static com.kispl.vgsrohiniupdate.Activities.Teacher.Dashboard.TeacherDashboardActivity.getNewsData;
import static com.kispl.vgsrohiniupdate.Activities.Teacher.Dashboard.TeacherDashboardActivity.getNoticeData;

/**
 * Created by Rishabh on 30/10/18.
 */
public class NewsListActivity extends AppCompatActivity
{
    RecyclerView recyclerView;
    NewsListAdapter newsListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_list_activity);
        init();
    }
    private void init()
    {
        Toolbar toolbar= findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.news_list_recycler_view);

        newsListAdapter = new NewsListAdapter(getNewsData(),NewsListActivity.this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(NewsListActivity.this);
        recyclerView.setAdapter(newsListAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.login_back);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
