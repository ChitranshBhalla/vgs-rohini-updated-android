package com.kispl.vgsrohiniupdate.Activities.Logout;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.kispl.vgsrohiniupdate.Activities.Login.LoginActivity;
import com.kispl.vgsrohiniupdate.Activities.Teacher.Dashboard.TeacherDashboardActivity;
import com.kispl.vgsrohiniupdate.Constants.Constant;
import com.kispl.vgsrohiniupdate.PrefrenceHelper.Configuration;
import com.kispl.vgsrohiniupdate.R;
import com.kispl.vgsrohiniupdate.Webservices.Logout.LogoutWebservice;

/**
 * Created by Rishabh on 29/10/18.
 */
public class Logout extends AsyncTask<String,String,String>
{

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ProgressDialog progressDialog;

    public Logout(Context context)
    {
        this.context = context;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Logging out");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        LogoutWebservice logoutWebservice = new LogoutWebservice();
        return logoutWebservice.logout(strings[0],strings[1]);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try
        {
            if(s.equalsIgnoreCase(Constant.WS_SUCCESS))
            {
                Configuration.setSharedPrefrenceValue(context,Constant.PREFS_NAME,Constant.userID,"");
                Configuration.setSharedPrefrenceValue(context,Constant.PREFS_NAME,Constant.userType,"");
                Configuration.setSharedPrefrenceValue(context,Constant.PREFS_NAME,Constant.userImage,"");
                Configuration.setSharedPrefrenceValue(context,Constant.PREFS_NAME,Constant.userEmail,"");
                Configuration.setSharedPrefrenceValue(context,Constant.PREFS_NAME,Constant.userName,"");

                Intent intent=new Intent(context, LoginActivity.class);
                context.startActivity(intent);
            }
            else
            {
                Toast.makeText(context, R.string.try_again, Toast.LENGTH_SHORT).show();
            }

        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {
            progressDialog.dismiss();
        }
    }
}
