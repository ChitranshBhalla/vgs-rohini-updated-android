package com.kispl.vgsrohiniupdate.Activities.News.NewsDetail;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kispl.vgsrohiniupdate.Activities.News.NewsList.NewsListActivity;
import com.kispl.vgsrohiniupdate.Activities.Notice.NoticeList.NoticeListActivity;
import com.kispl.vgsrohiniupdate.Beans.NewsBeans.NewsBeans;
import com.kispl.vgsrohiniupdate.R;
import com.squareup.picasso.Picasso;

import static com.kispl.vgsrohiniupdate.Utlities.HelpingFunctions.htmlToText;

/**
 * Created by Rishabh on 30/10/18.
 */
public class NewsDetailActivity extends AppCompatActivity {

    ImageView backImage,headerImage;
    TextView addedDate,newsTitle,newsDetail;
    String str_title,str_image,str_date,str_detail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_detail_activity);
        init();


    }
    private void init()
    {
        backImage = findViewById(R.id.back_image);
        headerImage = findViewById(R.id.news_header_image);
        addedDate = findViewById(R.id.news_added_date);
        newsTitle = findViewById(R.id.news_title_text);
        newsDetail = findViewById(R.id.news_detail_text);
        setupViews();
    }
    private void setupViews()
    {
        Intent intent = getIntent();
        str_title = intent.getStringExtra("news_title");
        str_detail = intent.getStringExtra("news_detail");
        str_date = intent.getStringExtra("news_date");
        str_image = intent.getStringExtra("news_image");

        newsTitle.setText(str_title);
        addedDate.setText(getResources().getString(R.string.added_on,str_date));
        Picasso.with(NewsDetailActivity.this).load(str_image).into(headerImage);
        newsDetail.setText(htmlToText(str_detail));
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }



}
